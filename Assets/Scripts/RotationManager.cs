﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationManager : MonoBehaviour
{
    [SerializeField] Transform objectToLook;

    [SerializeField] Transform pivot;
    [SerializeField] Transform baseCanon;

    [SerializeField] float rotationSpeedPivot;
    [SerializeField] float rotationSpeedCanon;


    // Update is called once per frame
    void Update()
    {
        Vector3 targetY = objectToLook.position;
        targetY.y = 0;

        Vector3 targetX= objectToLook.position;
        targetX.x = 0;

        Quaternion bc = Quaternion.identity;

        Vector3 pingV3 = targetY;
        Vector3 source = baseCanon.position;

        bc.SetLookRotation(pingV3 - source, Vector3.up);
        baseCanon.rotation = Quaternion.Slerp(baseCanon.rotation, bc, Time.deltaTime * rotationSpeedCanon);

        Quaternion p = Quaternion.identity;

        pingV3 = targetX;
        source = pivot.position;

        p.SetLookRotation(pingV3 - source, Vector3.up);
        pivot.rotation = Quaternion.Slerp(pivot.rotation, p, Time.deltaTime * rotationSpeedPivot);
    }
}
