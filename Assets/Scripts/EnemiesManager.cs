﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesManager : MonoBehaviour
{
    [SerializeField] GameObject bombPrefab;
    [SerializeField] GameObject ghostPrefab;

    List<Bomb> bombsList;
    List<Ghost> ghostsList;

    float timeToNextBomb = 10;
    float timeToNextGhost = 5;

    public int GetBombsCount()
    {
        return bombsList.Count;
    }

    public int GetGhostsCount()
    {
        return ghostsList.Count;
    }

    public Bomb GetBomb(int index)
    {
        return bombsList[index];
    }

    public Ghost GetGhost(int index)
    {
        return ghostsList[index];
    }

    public void TakeOutBombInList(GameObject bomb)
    {
        bombsList.Remove(bomb.GetComponent<Bomb>());
    }

    public void TakeOutGhostInList(GameObject ghost)
    {
        ghostsList.Remove(ghost.GetComponent<Ghost>());
    }

    void Start()
    {
        bombsList = new List<Bomb>();
        ghostsList = new List<Ghost>();
    }

    void Update()
    {
        Debug.Log("enemies manager");
        timeToNextBomb -= Time.deltaTime;
        timeToNextGhost -= Time.deltaTime;

        if (timeToNextBomb <= 0)
        {
            GameObject bomb = Instantiate(bombPrefab, Vector3.zero, Quaternion.identity, transform);

            bombsList.Add(bomb.GetComponent<Bomb>());

            timeToNextBomb = 10;
        }

        if (timeToNextGhost <= 0)
        {
            GameObject ghost = Instantiate(ghostPrefab, Vector3.zero, Quaternion.identity, transform);
        
            ghostsList.Add(ghost.GetComponent<Ghost>());
        
            timeToNextGhost = 5;
        }
    }
}
