﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public static string GetSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("Game");
        GameManager.GetInstance().ChangeScene(GameManager.scenes.game);        
    }

    public void GoToCredits()
    {
        SceneManager.LoadScene("Credits");
        GameManager.GetInstance().ChangeScene(GameManager.scenes.credits);        
    }

    public void GoToEndScene()
    {
        SceneManager.LoadScene("EndScene");
        GameManager.GetInstance().ChangeScene(GameManager.scenes.endScene);        
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
        GameManager.GetInstance().ChangeScene(GameManager.scenes.menu);        
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
