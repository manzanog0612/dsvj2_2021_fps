﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UI;
using UnityEngine;

public class BarManager : MonoBehaviour
{ 
    void Update()
    {
        PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();

        GetComponent<Slider>().value = pm.GetLife();
    }
}
