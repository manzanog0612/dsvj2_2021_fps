﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    Collider bombCollider;

    short damage = 50;
    public short score = 100;
    float timeToExplode = 2;
    bool activateTimer = false;

    GameObject target;

    void Start()
    {        
        bombCollider = GetComponent<Collider>();
        SpawnManager sp = GameObject.FindWithTag("SpawnManager").GetComponent<SpawnManager>();

        transform.position = sp.GetValidRandomPos(bombCollider, SpawnManager.colliderType.bomb);

        gameObject.tag = "Bomb";
    }

    void Update()
    {
        Debug.Log("bomb");
        if (activateTimer)
            timeToExplode -= Time.deltaTime;

        if (timeToExplode <= 0)
        {
            EnemiesManager em = GameObject.FindWithTag("EnemiesManager").GetComponent<EnemiesManager>();
            em.TakeOutBombInList(gameObject);
            target.GetComponent<PlayerManager>().AddDamageToPlayer(damage);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            activateTimer = true;
            target = other.gameObject;
        }
    }
}
