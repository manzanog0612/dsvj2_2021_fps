﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] Vector3[] spawnCircle;
    //x and z will be the position of the circle, y is the radius of each circle

    public enum colliderType
    {
        bomb,
        rewardBox,
        ghost
    }

    public Vector2 GetRandomizedInitialPos(int aux)
    {
        return Random.insideUnitCircle * spawnCircle[aux].y;
    }

    public Vector3 GetSpawnCircle(int index)
    {
        return spawnCircle[index];
    }    

    bool CheckMinimunDistance(Collider c1, Collider c2, Vector3 p)
    {
        Vector3 c1PointCloserToB = c1.ClosestPoint(p);
        Vector3 c2PointCloserToA = c2.ClosestPoint(c1PointCloserToB);

        return Vector3.Distance(c1PointCloserToB, c2PointCloserToA) > 0f;
    }

    public bool CheckOtherObjectInPos(Vector3 pos, Collider c, colliderType ct)
    {
        GameplayObjectsManager gom = GameObject.FindWithTag("GameplayObjectsManager").GetComponent<GameplayObjectsManager>();
        EnemiesManager em = GameObject.FindWithTag("EnemiesManager").GetComponent<EnemiesManager>();
        PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();

        int boxesCount = gom.GetRewardBoxesCount();
        int bombsCount = em.GetBombsCount();
        //int ghostsCount = em.GetGhostsCount();

        switch (ct)
        {
            case colliderType.bomb:
                bombsCount--;
                break;
            case colliderType.rewardBox:
                boxesCount--;
                break;
            case colliderType.ghost:
                //ghostsCount--;
                break;
            default:
                break;
        }

        float minimunDistanceFromPlayer = 5;

        for (int i = 0; i < boxesCount; i++)
        {
            RewardBox rb = gom.GetRewardBox(i);

            if (!CheckMinimunDistance(c, rb.GetComponent<Collider>(), rb.transform.position))
                return true;
        }

        for (int i = 0; i < bombsCount; i++)
        {
            Bomb b = em.GetBomb(i);

            if (!CheckMinimunDistance(c, b.GetComponent<Collider>(), b.transform.position))
                return true;
        }

        //for (int i = 0; i < ghostsCount; i++)
        //{
        //    Ghost g = em.GetGhost(i);
        //
        //    if (!CheckMinimunDistance(c, g.GetComponent<Collider>(), g.transform.position))
        //        return true;
        //}

        if (Vector3.Distance(pos, pm.transform.position) < minimunDistanceFromPlayer)
            return true;

        return false;
    }

    public Vector3 GetValidRandomPos(Collider c, colliderType ct)
    {
        Vector3 pos;
        Vector2 randomPos;

        //do
        //{
            int aux = Random.Range(1, 3);

            randomPos = GetRandomizedInitialPos(aux);

            pos = new Vector3(GetSpawnCircle(aux).x + randomPos.x, 1, GetSpawnCircle(aux).z + randomPos.y);

        //} while (CheckOtherObjectInPos(pos, c, ct));

        return pos;
    }
}
