﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    void Start()
    {
        name = "Ball";
    }

    void Update()
    {
        if (transform.position.y < 0.2f)
        {
            gameObject.GetComponentInParent<ShotsManager>().RemoveBallFromBallList(this);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ghost"))
        {
            other.gameObject.GetComponent<Ghost>().AddDamage(10);
            gameObject.GetComponentInParent<ShotsManager>().RemoveBallFromBallList(this);
            Destroy(gameObject);
        }
    }
}
