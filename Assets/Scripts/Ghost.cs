﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    private enum enemyStates
    {
        idle,
        going,
        followinPlayer,
        last
    }

    [SerializeField] int damage = 10;
    [SerializeField] float speed = 5;
    [SerializeField] float minTargetDistance = 1f;
    [SerializeField] float timeToMove = 1;
    [SerializeField] float distanceNearPlayer = 15;
    [SerializeField] int life = 30;

    int score = 200;

    Collider ghostCollider;

    Vector3 targetPos;
    Vector3 spawnCircle;

    enemyStates currentState = enemyStates.idle;

    Vector3 dir = Vector3.zero;

    public void AddDamage(int damage)
    {
        life -= damage;
    }

    bool CheckPlayerNear()
    {
        Transform player = GameObject.FindWithTag("Player").GetComponent<Transform>();

        return Vector3.Distance(transform.position, player.position) < distanceNearPlayer;
    }

    void Start()
    {
        ghostCollider = GetComponent<Collider>();
        SpawnManager sp = GameObject.FindWithTag("SpawnManager").GetComponent<SpawnManager>();

        spawnCircle = sp.GetSpawnCircle(Random.Range(1, 3));

        transform.position = sp.GetValidRandomPos(ghostCollider, SpawnManager.colliderType.ghost);

        gameObject.tag = "Ghost";
    }

    void Update()
    {
        Debug.Log("ghost");
        switch (currentState)
        {
            case enemyStates.idle:
                { 
                    timeToMove -= Time.deltaTime;
                    if (timeToMove <= 0)
                    {
                        targetPos = spawnCircle + new Vector3(Random.Range(1, spawnCircle.y), 0, Random.Range(1, spawnCircle.y));
                        targetPos.y = transform.position.y;

                        dir = (targetPos - transform.position).normalized;

                        currentState = enemyStates.going;
                        timeToMove = Random.Range(1, 2) + Random.Range(1, 9) / 10;
                    } 
                }
                break;
            case enemyStates.going:
                {
                    //transform.position = Vector3.Lerp(transform.position, targetPos, speed * Time.deltaTime);
                    transform.position += dir * speed * Time.deltaTime;

                    if (Vector3.Distance(transform.position, targetPos) < minTargetDistance)
                    { 
                        currentState = enemyStates.idle;
                    }
                }
                break;
            case enemyStates.followinPlayer:
                {
                    Transform player = GameObject.FindWithTag("Player").GetComponent<Transform>();

                    dir = (player.position - transform.position).normalized;

                    transform.position += dir * speed * Time.deltaTime;

                    transform.LookAt(player);

                    if (!CheckPlayerNear())
                    {
                        currentState = enemyStates.going;
                        dir = (targetPos - transform.position).normalized;
                    }
                }
                break;
            default:
                break;
        }

        if (CheckPlayerNear())
        { 
            currentState = enemyStates.followinPlayer; 
        }

        if (life <= 0)
        {
            PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();

            GetComponentInParent<EnemiesManager>().TakeOutGhostInList(gameObject);

            pm.AddScoreToPlayer(score);

            pm.AddItem(PlayerManager.amountItems.ghosts);

            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("ghost");

        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<PlayerManager>().AddDamageToPlayer(damage);
        }
    }
}
