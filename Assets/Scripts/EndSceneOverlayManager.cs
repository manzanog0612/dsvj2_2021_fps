﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndSceneOverlayManager : MonoBehaviour
{
    [SerializeField] Text resultText;
    [SerializeField] Text dataText;

    public enum amountItems
    {
        bombs,
        ghosts,
        rewardBoxes,
        last
    }

    void Start()
    {
        Cursor.visible = true; 
        Cursor.lockState = CursorLockMode.None;

        GameManager.FinalData fd = GameManager.GetInstance().GetFinalData();

        if (fd.win)
            resultText.text = "You won!";
        else
            resultText.text = "You lose!";

        GetComponentInChildren<Slider>().value = fd.life;

        dataText.text = "Life\n\nScore\n" + fd.score + "\nHighest score\n" + fd.highScore + "\nBullets Left\n" + fd.bullets + 
                        "\nChargers left\n" + fd.chargers + "\nDestroyed bombs\n" + fd.items[(int)amountItems.bombs] +
                        "\nKilled ghosts\n" + fd.items[(int)amountItems.ghosts] +
                        "\nReward boxes taken\n" + fd.items[(int)amountItems.rewardBoxes];
    }
}
