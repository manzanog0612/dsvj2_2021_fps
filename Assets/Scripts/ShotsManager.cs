﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotsManager : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] public short totalBullets;
    [SerializeField] public short totalChargers;
    [SerializeField] public short bulletsPerCharger;
    [SerializeField] MeshRenderer mr;

    [SerializeField] Transform shotStart;
    [SerializeField] GameObject ballPrefab;
    [SerializeField] float ballInitialSpeed = 30; 

    bool normalGun = true;
    List<Ball> ballList;

    public bool CheckNoMoreBullets() 
    {
        return totalChargers == 0 && totalBullets == 0;
    }

    bool CheckBombHit(GameObject go)
    {
        return go.tag == "Bomb";
    }

    public void SetShot()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(mousePos);
        RaycastHit hit;
        float maxHitDistance = 5f;

        if (Physics.Raycast(ray, out hit, maxHitDistance))
        {
            if (CheckBombHit(hit.transform.gameObject))
            {
                GameObject.FindWithTag("EnemiesManager").GetComponent<EnemiesManager>().TakeOutBombInList(hit.transform.gameObject);
                //I take out the hitten bomb from the bomb list

                GetComponent<PlayerManager>().AddScoreToPlayer(hit.transform.gameObject.GetComponent<Bomb>().score);
                //I give the player the score for breaking the bomb

                GetComponent<PlayerManager>().AddItem(PlayerManager.amountItems.bombs);
                //I add 1 to the amount of the item to the player

                Destroy(hit.transform.gameObject);
                //I destroy the bomb
            }
        }
    }

    void NormalGunUpdate()
    {
        if (Input.GetMouseButtonDown(0) && totalBullets > 0)
        {
            SetShot();
            totalBullets--;
        }
    }

    void Recharge()
    {
        if (Input.GetKeyDown(KeyCode.R) && normalGun)
        {
            totalBullets = bulletsPerCharger;
            totalChargers--;
        }
    }

    void BallGunUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject ball = Instantiate(ballPrefab, shotStart.position, Quaternion.identity, transform);
            ball.transform.rotation = shotStart.rotation;
            ball.GetComponent<Rigidbody>().velocity = ball.transform.forward * ballInitialSpeed;
        }
    }

    public void RemoveBallFromBallList(Ball b)
    {
        ballList.Remove(b);
    }

    void Start()
    {
        ballList = new List<Ball>();
        PlayerManager.shootUpdate = NormalGunUpdate;
        PlayerManager.shootUpdate += Recharge;
    }

    void Update()
    {
        Debug.Log("shots manager");
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            PlayerManager.shootUpdate = NormalGunUpdate;
            PlayerManager.shootUpdate += Recharge;
            normalGun = true;
            mr.material.color = Color.black;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            PlayerManager.shootUpdate = BallGunUpdate;
            normalGun = false;
            mr.material.color = Color.green;
        }              
    }    
}
