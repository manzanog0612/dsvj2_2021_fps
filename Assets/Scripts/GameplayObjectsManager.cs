﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayObjectsManager : MonoBehaviour
{
    [SerializeField] GameObject rewardBoxPrefab;

    List<RewardBox> rewardBoxesList;

    float timeToNextRewardBox = 20;

    public int GetRewardBoxesCount()
    {
        return rewardBoxesList.Count;
    }

    public RewardBox GetRewardBox(int index)
    {
        return rewardBoxesList[index];
    }

    public void TakeOutBoxInList(GameObject box)
    {
        rewardBoxesList.Remove(box.GetComponent<RewardBox>());
    }

    void Start()
    {
        rewardBoxesList = new List<RewardBox>();
    }

    void Update()
    {
        Debug.Log("box Manager");
        timeToNextRewardBox -= Time.deltaTime;

        if (timeToNextRewardBox <= 0)
        {
            GameObject box = Instantiate(rewardBoxPrefab, Vector3.zero, Quaternion.identity, transform);

            rewardBoxesList.Add(box.GetComponent<RewardBox>());

            timeToNextRewardBox = 20;
        }
    }
}
