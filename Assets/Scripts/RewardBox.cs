﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardBox : MonoBehaviour
{
    Collider boxCollider;

    public int score;

    /*bool CheckOtherObjectInPos(Vector3 pos)
    {
        GameplayObjectsManager gom = GameObject.FindWithTag("GameplayObjectsManager").GetComponent<GameplayObjectsManager>();
        EnemiesManager em = GameObject.FindWithTag("EnemiesManager").GetComponent<EnemiesManager>();
        PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();

        int boxesCount = gom.GetRewardBoxesCount();
        int bombsCount = em.GetBombsCount();

        float minimunDistanceFromPlayer = 5;

        for (int i = 0; i < bombsCount; i++)
        {
            Bomb b = em.GetBomb(i);

            if (!CheckMinimunDistance(boxCollider, b.GetComponent<Collider>(), b.transform.position))
                return true;
        }

        for (int i = 0; i < boxesCount - 1; i++)
        {
            RewardBox rb = gom.GetRewardBox(i);

            if (!CheckMinimunDistance(boxCollider, rb.GetComponent<Collider>(), rb.transform.position))
                return true;
        }

        if (Vector3.Distance(pos, pm.transform.position) < minimunDistanceFromPlayer)
            return true;

        return false;
    }*/

    void Start()
    {
        boxCollider = GetComponent<Collider>();
        score = Random.Range(20, 50);

        SpawnManager sp = GameObject.FindWithTag("SpawnManager").GetComponent<SpawnManager>();

        transform.position = sp.GetValidRandomPos(boxCollider, SpawnManager.colliderType.rewardBox);

        gameObject.tag = "RewardBox";
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("box");
        if (other.gameObject.tag == "Player")
        {
            GameObject.FindWithTag("GameplayObjectsManager").GetComponent<GameplayObjectsManager>().TakeOutBoxInList(gameObject);
            //I take out the taken box from the box list
            other.gameObject.GetComponent<PlayerManager>().AddScoreToPlayer(score);
            //I give the player the score for taking the box
            other.gameObject.GetComponent<PlayerManager>().AddItem(PlayerManager.amountItems.rewardBoxes);
            //I add 1 to the amount of the item to the player
            Destroy(gameObject);
            //I destroy the box
        }
    }
}
