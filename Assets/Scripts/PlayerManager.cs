﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
   [SerializeField] int life = 100;
   [SerializeField] int score = 0;
   [SerializeField] int[] items;

    public delegate void Shoot();
    public static Shoot shootUpdate;

    public enum amountItems
    {    
        bombs,
        ghosts,
        rewardBoxes,
        last
    }

    private void Start()
    {
        items = new int[(int)amountItems.last];
    }

    public void AddItem(amountItems item)
    {
        items[(int)item]++;
    }

    public bool GetAlive()
    {
        return life > 0;
    }

    public int GetLife()
    {
        return life;
    }
    public int GetScore()
    {
        return score;
    }

    public int[] GetItems()
    {
        return items;
    }

    public void AddDamageToPlayer(int damage)
    {
        life -= damage;
    }

    public void AddScoreToPlayer(int s)
    {
        score += s;
    }

    private void Update()
    {
        shootUpdate?.Invoke();
    }


}
