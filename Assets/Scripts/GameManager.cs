﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager GetInstance()
    {
        return instance;
    }

    int highScore = 0;

    public enum scenes
    {
        menu, 
        game,
        endScene,
        credits
    }

    public void ChangeScene(scenes s)
    {
        actualScene = s;
    }

    scenes actualScene = scenes.menu;

    enum gameplayState
    {
        on,
        win,
        lose,
        last
    }

    gameplayState currentGameplayState = gameplayState.on;

    public class FinalData
    {
        public short bullets;
        public short chargers;
        public int score;
        public int highScore;
        public int life;
        public int[] items;
        public bool win;
    }

    public FinalData SetFinalData()
    {
        PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        ShotsManager sm = GameObject.FindWithTag("Player").GetComponent<ShotsManager>();

        FinalData fd = new FinalData();

        fd.bullets = sm.totalBullets;
        fd.chargers = sm.totalChargers;
        fd.life = pm.GetLife();
        fd.score = pm.GetScore();
        fd.highScore = fd.score;
        fd.items = pm.GetItems();

        if (fd.score > highScore)
        {
            highScore = fd.score;
        }

        fd.highScore = highScore;

        fd.win = (currentGameplayState == gameplayState.win);

        return fd;
    }

    public FinalData GetFinalData()
    {
        return fData;
    }

    FinalData fData;     

    void SetEndScene()
    {
        SceneChanger sc = new SceneChanger();

        SetFinalData();
        sc.GoToEndScene();
        fData = SetFinalData();
    }

    public void LoseGame()
    {
        currentGameplayState = gameplayState.lose;
    }

    public void WinGame()
    {
        currentGameplayState = gameplayState.win;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log(name + " has been destroyed, there's already one Game Manager created.");
            return;
        }
        instance = this;
        Debug.Log(name + " has been created and is now the Game Manager.");
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        Debug.Log("spawnManager");

        switch (actualScene)
        {
            case scenes.menu:   
                if (currentGameplayState != gameplayState.on)
                {
                    currentGameplayState = gameplayState.on;
                } 
                break;
            case scenes.game:
                {
                    GameObject p = GameObject.FindWithTag("Player");

                    if (p != null)
                    {
                        if (!p.GetComponent<PlayerManager>().GetAlive())
                            LoseGame();

                        if (p.GetComponent<ShotsManager>().CheckNoMoreBullets())
                            WinGame();
                    }

                    if (currentGameplayState == gameplayState.win || currentGameplayState == gameplayState.lose)
                    {
                        SetEndScene();
                    }
                }
                break;
            case scenes.endScene:
                break;
            case scenes.credits:
                break;
            default:
                break;
        }
    }
}
