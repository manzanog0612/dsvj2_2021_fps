﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OverlayManager : MonoBehaviour
{
    [SerializeField] Text UIText;

    void Update()
    {
        PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        ShotsManager sm = GameObject.FindWithTag("Player").GetComponent<ShotsManager>();

        UIText.text = "Life\nScore: " + pm.GetScore() + "\nBullets: " + sm.totalBullets + "\nChargers left: " + sm.totalChargers;
    }
}
